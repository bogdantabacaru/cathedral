from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QWidget

from ui.game_widget import Ui_GameWidget

from player import Human


class GameWidget(QWidget):

    def __init__(self, parent, player1, player2):
        super().__init__(parent)

        self.player1 = player1
        self.player2 = player2

        self.active_player = player1
        self.next_player = player2

        parent.ui.actionRestart.setEnabled(True)
        parent.ui.actionUndo.setEnabled(True)
        parent.ui.actionRedo.setEnabled(True)
        parent.ui.actionSave.setEnabled(True)
        parent.ui.actionSave_as.setEnabled(True)

        self.ui = Ui_GameWidget()
        self.ui.setupUi(self)
        self.ui.board_frame.setFocus()

        self.ui.player1_name_label.setText(self.active_player.name)
        self.ui.player2_name_label.setText(self.next_player.name)

        self.active_player.inventory_frame_ref = self.ui.player1_inventory_frame
        self.next_player.inventory_frame_ref = self.ui.player2_inventory_frame

        self.active_player.score_label_ref = self.ui.player1_score_label
        self.next_player.score_label_ref = self.ui.player2_score_label

    def play_turn(self):
        self.active_player.play_turn(self.ui.board_frame)

        self.ui.board_frame.evaluate_turn()

        # Update the scores
        # self.active_player.update_and_display_score(self.next_player.inventory_frame_ref)
        self.next_player.update_and_display_score(self.active_player.inventory_frame_ref)

        # If the next player cannot play, skip their turn
        # If neither player can play, end the game
        if self.next_player.can_play(self.ui.board_frame):
            self.change_player()
        elif not self.active_player.can_play(self.ui.board_frame):
            self.end_game()

    def change_player(self):
        self.active_player, self.next_player = self.next_player, self.active_player

    def end_game(self):
        if self.active_player.score == self.next_player.score:
            message = 'It\'s a draw!'
        elif self.active_player.score > self.next_player.score:
            message = self.active_player.name + ' wins!'
        else:
            message = self.next_player.name + ' wins!'

        self.parent().update_status_bar_message(message)

    def human_game_keyboard_strokes(self, board_frame, e):
        if board_frame.active_piece is None:
            return

        key = e.key()
        modifiers = e.modifiers()

        if key == Qt.Key_Left:
            board_frame.try_to_move_active_piece(-1, 0)
        elif key == Qt.Key_Right:
            board_frame.try_to_move_active_piece(1, 0)
        elif key == Qt.Key_Up:
            board_frame.try_to_move_active_piece(0, -1)
        elif key == Qt.Key_Down:
            board_frame.try_to_move_active_piece(0, 1)
        elif (modifiers & Qt.ShiftModifier) and key == Qt.Key_R:
            board_frame.try_to_rotate_active_piece(-1)
        elif key == Qt.Key_R:
            board_frame.try_to_rotate_active_piece(1)
        elif key == Qt.Key_Return or key == Qt.Key_Enter:
            self.play_turn()
        elif key == Qt.Key_Escape:
            board_frame.reset_active_piece()


class HumanVsHumanGameWidget(GameWidget):

    def __init__(self, parent, player1, player2):
        super().__init__(parent, player1, player2)

        self.ui.player1_inventory_frame.setEnabled(False)
        self.ui.player2_inventory_frame.setEnabled(False)

        self.ui.board_frame.create_cathedral_active_piece()

        self.parent().update_status_bar_message(f'{self.active_player.name}\'s turn')

    def play_turn(self):
        if not self.active_player.is_placement_valid(self.ui.board_frame, open_message_box=True):
            return

        super().play_turn()

    def change_player(self):
        super().change_player()

        self.parent().update_status_bar_message(f'{self.active_player.name}\'s turn')

        self.active_player.inventory_frame_ref.setEnabled(True)
        self.next_player.inventory_frame_ref.setEnabled(False)

    def keyPressEvent(self, e):
        super().keyPressEvent(e)

        self.human_game_keyboard_strokes(self.ui.board_frame, e)

        self.update()


class CpuVsCpuGameWidget(GameWidget):

    def __init__(self, parent, player1, player2):
        super().__init__(parent, player1, player2)

        self.timer = QTimer()
        self.timer.timeout.connect(lambda: self.play_turn())

        # Play the first turn before starting the timer
        # Does not work because for whatever reason, the inventory frame widgets are not visible
        # self.play_turn()

        self.paused = False
        self.start_timer()

    def start_timer(self, time=1000):
        self.timer.start(time)

    def play_turn(self):
        self.parent().update_status_bar_message(f'{self.active_player.name}\'s turn')

        super().play_turn()

    def end_game(self):
        self.timer.stop()

        super().end_game()

    def toggle_timer(self, time=1000):
        if self.paused:
            self.parent().update_status_bar_message(f'{self.active_player.name}\'s turn')
            self.paused = False
            self.timer.start(time)
        else:
            self.parent().update_status_bar_message('Paused')
            self.paused = True
            self.timer.stop()

    def keyPressEvent(self, e):
        # super().keyPressEvent(e)

        key = e.key()

        if key == Qt.Key_Space:
            self.toggle_timer()

        self.update()


class HumanVsCpuGameWidget(GameWidget):

    def __init__(self, parent, player1, player2):
        super().__init__(parent, player1, player2)

        self.timer = QTimer()
        self.timer.timeout.connect(lambda: self.play_turn())

        cpu = self.next_player if isinstance(self.active_player, Human) else self.active_player
        cpu.inventory_frame_ref.setEnabled(False)

        if isinstance(self.active_player, Human):
            self.ui.board_frame.create_cathedral_active_piece()
            self.play_turn()
        else:
            self.timer.start(1000)

    def play_turn(self):
        self.timer.stop()

        self.parent().update_status_bar_message(f'{self.active_player.name}\'s turn')

        if isinstance(self.active_player, Human):
            if not self.active_player.is_placement_valid(self.ui.board_frame, open_message_box=True):
                return

        super().play_turn()

        if not isinstance(self.active_player, Human):
            self.timer.start(1000)

    def keyPressEvent(self, e):
        super().keyPressEvent(e)

        self.human_game_keyboard_strokes(self.ui.board_frame, e)

        self.update()


class CpuVsHumanGameWidget(HumanVsCpuGameWidget):
    ...
