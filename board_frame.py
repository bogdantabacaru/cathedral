import numpy as np

from PyQt5.QtGui import QPainter, QBrush, QPen, QColor
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QFrame

from piece_label import PieceLabel
from cathedral_piece_label import CathedralPieceLabel


class BoardFrame(QFrame):

    def __init__(self, parent):
        super().__init__(parent)

        self.grid_width = 10
        self.grid_height = 10
        self.tile_width = 40
        self.tile_height = 40

        self.active_piece = None
        self.player1_regions = []
        self.player2_regions = []

        self.contents = {(i, j): None for i in range(self.grid_height) for j in range(self.grid_width)}

    def place(self):
        for i, j in np.ndindex(*self.active_piece.shape.shape):
            if self.active_piece.shape[i, j]:
                self.contents[self.active_piece.x0 + j, self.active_piece.y0 + i] = self.active_piece

        # self.print_contents()

    def print_contents(self):
        print('Contents')
        for i in range(self.grid_height):
            row = []
            for j in range(self.grid_width):
                if self.contents[j, i] is None:
                    row.append('0')
                elif self.contents[j, i].name == 'cathedral':
                    row.append('c')
                else:
                    row.append(self.contents[j, i].player)
            print(', '.join(row))

    def try_to_move_active_piece(self, x=0, y=0):
        if not self.active_piece.is_out_of_bounds(self, x, y):
            self.active_piece.relocate(x, y)
            self.active_piece.move(self.tile_height * self.active_piece.x0, self.tile_width * self.active_piece.y0)

    def clear_active_piece(self):
        self.active_piece = None

    def reset_active_piece(self):
        if self.active_piece is None:
            return

        if self.active_piece.name == 'cathedral':
            return

        self.active_piece.hide()

        if self.active_piece.inventory_ref is not None:
            self.active_piece.inventory_ref.show()

        self.clear_active_piece()

    def create_active_piece(self, piece):
        if self.active_piece is not None:
            x0, y0 = self.active_piece.x0, self.active_piece.y0
        else:
            x0 = y0 = 0

        self.reset_active_piece()

        self.active_piece = PieceLabel(self)
        self.active_piece.setPixmap(piece.pixmap())
        self.active_piece.setScaledContents(True)
        self.active_piece.setObjectName(piece.objectName())
        [self.active_piece.setProperty(p, piece.property(p)) for p in ['is_symmetric']]
        self.active_piece.inventory_ref = piece

        # If replacing an already-selected active piece, use the existing (x0, y0) coordinates
        self.active_piece.relocate(x0, y0)
        dx, dy = self.active_piece.nudge(self)
        if dx or dy:
            self.active_piece.relocate(dx, dy)

        height, width = piece.shape.shape
        tile_height, tile_width = self.tile_width, self.tile_height
        # New nudged coordinates
        x0, y0 = self.active_piece.x0, self.active_piece.y0
        self.active_piece.setGeometry(x0 * tile_width, y0 * tile_height, width * tile_width, height * tile_height)

        self.active_piece.show()

        return self.active_piece

    def create_cathedral_active_piece(self):
        self.active_piece = CathedralPieceLabel(self)
        return self.active_piece

    def print_visited(self, visited):
        print('Visited')
        for i in range(self.grid_height):
            row = []
            for j in range(self.grid_width):
                row.append(str(visited[j, i] * 1))
            print(', '.join(row))

    def try_to_rotate_active_piece(self, rotation_factor):
        self.active_piece.rotate(rotation_factor)
        dx, dy = self.active_piece.nudge(self)
        if dx or dy:
            self.active_piece.relocate(dx, dy)
            self.active_piece.move(self.tile_height * self.active_piece.x0, self.tile_width * self.active_piece.y0)

    def traverse(self, i, j, visited, player_name='1', other_player_name='2'):
        region = []

        # Stop traversing if we're going out of bounds
        if j < 0 or j >= self.grid_height or i < 0 or i >= self.grid_width:
            return region

        value = self.contents[i, j]

        # Stop traversing if we've visited this tile before or if we are hitting the player's wall
        if visited[i, j] or (value is not None and value.name != 'cathedral' and value.player == player_name):
            return region

        visited[i, j] = True
        region = [(i, j)]

        for di in [-1, 0, 1]:
            for dj in [-1, 0, 1]:
                if di == dj == 0:
                    continue

                region += self.traverse(i + di, j + dj, visited, player_name, other_player_name)

        return region

    def calculate_regions(self, player_name, other_player_name):
        """
        Determine which tiles of the same color (tiles which belong to the same player) form a contiguous region.
        Valid regions are surrounded entirely by the pieces of another player or are surrounded by a contiguous wall of
        the other player's pieces and one or more of the board's edges.
        A region is invalid if it contains a cathedral and one or more opponent's pieces.
        A region is invalid if it contains two or more of the opponent's pieces.
        """

        visited = {(i, j): False for i, j in self.contents}

        regions = []

        for i, j in self.contents:
            region = self.traverse(i, j, visited, player_name, other_player_name)
            if region:
                regions.append(region)

        # Validate regions
        # If only one region found, no valid wall has been created yet
        if len(regions) < 2:
            regions = []

        valid_regions = [False] * len(regions)

        for region_index, region in enumerate(regions):
            opponent_pieces = set()

            for i, j in region:
                value = self.contents[i, j]
                if value is None:
                    continue

                if value.name == 'cathedral' or value.player == other_player_name:
                    opponent_pieces.add(self.contents[i, j])

            valid_regions[region_index] = len(opponent_pieces) < 2

        regions = [region for i, region in enumerate(regions) if valid_regions[i]]

        # self.print_contents()
        # self.print_visited(visited)
        # print('len(regions)', len(regions))

        return regions

    def remove_pieces(self, regions, other_player_id):
        for region in regions:
            opponent_pieces = set()

            for i, j in region:
                value = self.contents[i, j]
                if value is None:
                    continue

                if value.name == 'cathedral' or value.player == other_player_id:
                    opponent_pieces.add(self.contents[i, j])

            if len(opponent_pieces) == 1:
                list(opponent_pieces)[0].remove()

    def evaluate_turn(self):
        # self.print_contents()

        # Update each player's regions (or territories)
        self.player1_regions = self.calculate_regions('1', '2')
        self.player2_regions = self.calculate_regions('2', '1')

        # Remove single pieces from within a region
        self.remove_pieces(self.player1_regions, '2')
        self.remove_pieces(self.player2_regions, '1')

        self.update()

    def paintEvent(self, a0):
        super().paintEvent(a0)

        painter = QPainter(self)
        black_pen = QPen(Qt.black, 1, Qt.SolidLine)

        # Draw the regions
        # Palette: https://coolors.co/aa9064-492531-78c0e0-183a37-bac9c8
        player1_color = QColor(73, 37, 49)  # Old Mauve
        player2_color = QColor(170, 144, 100)  # Camel

        height, width = self.tile_width, self.tile_height

        for regions, color in zip([self.player1_regions, self.player2_regions], [player1_color, player2_color]):
            brush = QBrush(color, Qt.SolidPattern)

            for region in regions:
                for x, y in region:
                    painter.setPen(black_pen)
                    painter.setBrush(brush)
                    painter.drawEllipse((x + 0.5) * height, (y + 0.5) * width, height / 10, width / 10)
