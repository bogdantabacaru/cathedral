from PyQt5.QtGui import QTransform
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QLabel

from piece import Piece


class PieceLabel(QLabel, Piece):

    def __init__(self, parent):
        super().__init__(parent)

    def setObjectName(self, name):
        super().setObjectName(name)

        self.set_shape(name)

    def rotate(self, rotation_factor=1):
        if self.property('is_symmetric'):
            return

        super().rotate(rotation_factor)

        # Ensure that the pixel map will retain its correct scaling factors
        if rotation_factor in [0, 2]:
            height, width = self.width(), self.height()
        else:
            height, width = self.height(), self.width()

        self.setGeometry(QRect(self.x(), self.y(), height, width))
        self.setPixmap(self.pixmap().transformed(QTransform().rotate(rotation_factor * 90)))

    def remove(self):
        # Hide the piece from the board frame
        self.hide()

        # Show the piece again in the inventory
        # Skip this step if we are processing the cathedral because it does not contain an inventory reference
        if self.inventory_ref is not None:
            self.inventory_ref.show()
