from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QRect

from piece_label import PieceLabel


class CathedralPieceLabel(PieceLabel):

    def __init__(self, parent):
        super().__init__(parent)

        self.setGeometry(QRect(0, 0, 120, 160))
        self.setPixmap(QPixmap(":/all/resources/pieces/player1/cathedral.png"))
        self.setScaledContents(True)
        self.setObjectName("player1_cathedral_label")
        self.show()
