import random

import numpy as np

from PyQt5.QtWidgets import QMessageBox

from piece import Piece


class Player:

    def __init__(self, name, is_player1):
        self.is_player1 = is_player1
        self.name = name
        self.score = 47
        self.inventory_frame_ref = None
        self.score_label_ref = None
        self.first_turn = True

    def play_turn(self, _):
        print(f'{self.name}\'s turn')

    def end_turn(self):
        if self.first_turn:
            self.first_turn = False

    def update_score(self, opponent_inventory):
        self.score = int(sum([piece.shape.sum() for piece in opponent_inventory.children() if piece.isVisible()]))

    def display_score(self):
        self.score_label_ref.setText(str(self.score))

    def update_and_display_score(self, opponent_inventory):
        self.update_score(opponent_inventory)
        self.display_score()

    def has_pieces(self):
        return self.inventory_frame_ref.children()

    def get_opponent_regions(self, board_frame):
        return board_frame.player2_regions if self.is_player1 else board_frame.player1_regions

    def is_placement_valid(self, board_frame, open_message_box=False):
        # Cannot place in an opponent's region
        opponent_regions = self.get_opponent_regions(board_frame)

        for i, j in np.ndindex(*board_frame.active_piece.shape.shape):
            coord = (board_frame.active_piece.x0 + j, board_frame.active_piece.y0 + i)

            if board_frame.active_piece.shape[i, j]:
                # Cannot place on top of other pieces
                if board_frame.contents[coord] is not None:
                    return False

                for region in opponent_regions:
                    if coord in region:
                        return False

        # Player2 is not allowed to create a region in their first turn
        if not self.is_player1 and self.first_turn:
            contents = board_frame.contents.copy()
            board_frame.place()
            opponent_regions = board_frame.calculate_regions('2', '1')
            board_frame.contents = contents

            if opponent_regions:
                if open_message_box:
                    msg_box = QMessageBox()
                    msg_box.setWindowTitle('Illegal Move - Cathedral Game')
                    msg = "You are not allowed to create a region during your first move."
                    msg_box.question(board_frame, '', msg, msg_box.Ok)

                return False

        return True

    def has_free_tiles(self, board_frame):
        """ Check if the player has empty tiles which do not belong to the opponent """

        opponent_regions = self.get_opponent_regions(board_frame)

        for (i, j), value in board_frame.contents.items():
            if value is not None:
                continue

            for opponent_region in opponent_regions:
                if (i, j) in opponent_region:
                    break
            else:
                return True

        return False

    def get_playable_pieces(self, board_frame, stop_at_first=False, second_player_first_piece=False):
        opponent_regions = self.get_opponent_regions(board_frame)

        playable_pieces = []

        for piece in self.inventory_frame_ref.children():
            if not piece.isVisible():
                continue

            # If the piece is not symmetric, rotate it up to 4 times and repeat the search to try out all positions
            n_rotations = 1 if piece.property('is_symmetric') else 4

            for rotation_factor in range(n_rotations):
                tmp_piece = Piece()
                tmp_piece.shape = np.copy(piece.shape)
                tmp_piece.rotate(rotation_factor)

                # Iterate through the board searching for a location to place the piece
                for di in range(board_frame.grid_height):
                    for dj in range(board_frame.grid_width):
                        for i, j in np.ndindex(*tmp_piece.shape.shape):
                            new_j = dj + j
                            new_i = di + i

                            # The piece must be within the board's bounds
                            if new_j < 0 or new_j >= board_frame.grid_width or \
                                    new_i < 0 or new_i >= board_frame.grid_height:
                                break

                            if not tmp_piece.shape[i, j]:
                                continue

                            # Must be an empty tile
                            if board_frame.contents[new_j, new_i] is not None:
                                break

                            # Cannot place within an opponent's region
                            is_in_opponent_region = False
                            for opponent_region in opponent_regions:
                                if (new_j, new_i) in opponent_region:
                                    is_in_opponent_region = True
                                    break

                            if is_in_opponent_region:
                                break
                        else:
                            if not second_player_first_piece:
                                playable_pieces.append([piece, dj, di, rotation_factor])
                            else:
                                # Player2 is not allowed to create any regions with their first piece
                                contents = board_frame.contents.copy()

                                for i, j in np.ndindex(*piece.shape.shape):
                                    if piece.shape[i, j]:
                                        contents[piece.x0 + j, piece.y0 + i] = piece

                                regions = board_frame.calculate_regions('2', '1')

                                if not regions:
                                    playable_pieces.append([piece, dj, di, rotation_factor])

                            if stop_at_first and playable_pieces:
                                return playable_pieces

        return playable_pieces

    def can_place_pieces(self, board_frame):
        """ Check if at least one piece can be placed on the board and return the coordinates of that placement """

        return self.get_playable_pieces(board_frame, stop_at_first=True)

    def can_play(self, board_frame):
        return self.has_pieces() and self.has_free_tiles(board_frame) and self.can_place_pieces(board_frame)


class EasyCpu(Player):
    def __init__(self, name, id_):
        super().__init__(name, id_)

    def play_turn(self, board_frame):
        super().play_turn(board_frame)

        if not board_frame.children():
            # Player1's first turn
            active_piece = board_frame.create_cathedral_active_piece()
            active_piece.rotate(random.randint(0, 3))
            max_y, max_x = active_piece.shape.shape
            x = random.randint(0, board_frame.grid_height - max_x)
            y = random.randint(0, board_frame.grid_width - max_y)
            board_frame.try_to_move_active_piece(x, y)
            board_frame.place()
            board_frame.clear_active_piece()
        else:
            p2_first_piece = len(board_frame.children()) == 1
            playable_pieces = self.get_playable_pieces(board_frame, second_player_first_piece=p2_first_piece)

            if playable_pieces:
                piece, x, y, rotation_factor = random.choice(playable_pieces)
                piece.hide()
                board_frame.create_active_piece(piece)
                board_frame.try_to_rotate_active_piece(rotation_factor)
                board_frame.try_to_move_active_piece(x, y)
                board_frame.place()
                board_frame.clear_active_piece()

        self.end_turn()


class HardCpu(EasyCpu):
    ...


class Human(Player):
    def __init__(self, name, id_):
        super().__init__(name, id_)

    def play_turn(self, board_frame):
        super().play_turn(board_frame)

        board_frame.place()
        board_frame.clear_active_piece()

        self.end_turn()


player_clss = {
    'Human': Human,
    'CPU': {
        'Easy': EasyCpu,
        'Hard': HardCpu,
    }
}


player_attributes = {
    'Human': ['Human', None],
    'EasyCpu': ['CPU', 'Easy'],
    'HardCpu': ['CPU', 'Hard'],
}


def create_player(type_, cpu_difficulty, name, player_id):
    if type_ == 'Human':
        cls = player_clss[type_]
    else:
        cls = player_clss[type_][cpu_difficulty]

    return cls(name, player_id)


def create_player1(type_, cpu_difficulty, name):
    return create_player(type_, cpu_difficulty, name, True)


def create_player2(type_, cpu_difficulty, name):
    return create_player(type_, cpu_difficulty, name, False)
