from PyQt5.QtCore import Qt

from piece_label import PieceLabel


class ClickablePieceLabel(PieceLabel):

    def __init__(self, parent):
        super().__init__(parent)

    def mouseReleaseEvent(self, a0):
        """ Select the next active piece to be placed on the board """

        # The cathedral cannot be clicked
        if self.name == 'cathedral':
            return

        # Only left mouse buttons allowed
        if a0.button() != Qt.LeftButton:
            return

        # Once placed, the piece disappears
        if not self.isVisible():
            return

        # Only' the active player's pieces are clickable
        if not self.isEnabled():
            return

        super().mouseReleaseEvent(a0)

        self.hide()

        # self --> player inventory frame --> central widget
        self.parent().parent().ui.board_frame.create_active_piece(self)
