from PyQt5.QtWidgets import QWidget

from ui.game_menu_widget import Ui_GameMenuWidget


class GameMenuWidget(QWidget):

    def __init__(self, parent):
        super().__init__(parent)

        parent.ui.actionRestart.setEnabled(False)
        parent.ui.actionUndo.setEnabled(False)
        parent.ui.actionRedo.setEnabled(False)
        parent.ui.actionSave.setEnabled(False)
        parent.ui.actionSave_as.setEnabled(False)

        self.ui = Ui_GameMenuWidget()
        self.ui.setupUi(self)

        self.ui.player1_type_combo_box.currentTextChanged.connect(
            lambda: self.toggle_cpu_difficulty_enabled(self.ui.player1_type_combo_box)
        )

        self.ui.player2_type_combo_box.currentTextChanged.connect(
            lambda: self.toggle_cpu_difficulty_enabled(self.ui.player2_type_combo_box)
        )

        self.ui.start_game_button.clicked.connect(
            lambda: parent.set_game_central_widget(
                self.ui.player1_type_combo_box.currentText(),
                self.ui.player1_cpu_difficulty_combo_box.currentText(),
                self.ui.player1_name_line_edit.text(),
                self.ui.player2_type_combo_box.currentText(),
                self.ui.player2_cpu_difficulty_combo_box.currentText(),
                self.ui.player2_name_line_edit.text(),
            )
        )

    def toggle_cpu_difficulty_enabled(self, obj):
        target = self.ui.player1_cpu_difficulty_label if obj == self.ui.player1_type_combo_box else \
            self.ui.player2_cpu_difficulty_label
        target.setEnabled(obj.currentText() == 'CPU')

        target = self.ui.player1_cpu_difficulty_combo_box if obj == self.ui.player1_type_combo_box else \
            self.ui.player2_cpu_difficulty_combo_box
        target.setEnabled(obj.currentText() == 'CPU')
