import sys

from PyQt5.QtGui import QFontDatabase
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QDialog

from ui.main_window import Ui_MainWindow
from ui.about_dialog import Ui_AboutDialog

from player import player_attributes, create_player1, create_player2
from game_menu_widget import GameMenuWidget
from game_widget import HumanVsHumanGameWidget, HumanVsCpuGameWidget, CpuVsHumanGameWidget, CpuVsCpuGameWidget

# TODO
# Use genetic algorithms and neural networks to teach the AI the best strategy for playing
# Fix the undo-redo feature
# Add save and load features

# Polishing
# Implement a Winner screen
# Add buttons to rotate and move pieces
# Implement drag and drop?
# Update graphics when maximizing the screen


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        QFontDatabase.addApplicationFont(':all/resources/fonts/Leckerli_One/LeckerliOne-Regular.ttf')

        self.setObjectName('Cathedral Game')

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.setFixedSize(self.size())

        self.ui.actionNew.triggered.connect(lambda: self.triggered_new())
        self.ui.actionRestart.triggered.connect(lambda: self.triggered_restart())
        self.ui.actionLoad.triggered.connect(lambda: self.triggered_load())
        self.ui.actionSave.triggered.connect(lambda: self.triggered_save())
        self.ui.actionSave_as.triggered.connect(lambda: self.triggered_save_as())
        self.ui.actionUndo.triggered.connect(lambda: self.triggered_undo())
        self.ui.actionRedo.triggered.connect(lambda: self.triggered_redo())
        self.ui.actionAbout.triggered.connect(lambda: self.triggered_about())

        self.show()

        # self.set_game_central_widget('CPU', 'Easy', 'Easy CPU', 'CPU', 'Hard', 'Hard CPU')
        self.set_game_menu_central_widget()

    def set_game_menu_central_widget(self):
        self.setCentralWidget(GameMenuWidget(self))

    def set_game_central_widget(self, p1_type, p1_cpu_difficulty, p1_name, p2_type, p2_cpu_difficulty, p2_name):
        player1 = create_player1(p1_type, p1_cpu_difficulty, p1_name)
        player2 = create_player2(p2_type, p2_cpu_difficulty, p2_name)

        if p1_type == p2_type == 'CPU':
            game_cls = CpuVsCpuGameWidget
        elif p1_type == p2_type == 'Human':
            game_cls = HumanVsHumanGameWidget
        elif p1_type == 'CPU' and p2_type == 'Human':
            game_cls = CpuVsHumanGameWidget
        else:
            game_cls = HumanVsCpuGameWidget

        self.setCentralWidget(game_cls(self, player1, player2))

    def update_status_bar_message(self, msg):
        self.ui.statusbar.showMessage(msg)

    def triggered_new(self):
        msg_box = QMessageBox()
        msg_box.setWindowTitle('New Game - Cathedral Game')
        msg = "Do you really want to start a new game?\nThe current progress will be lost."
        ret = msg_box.question(self, '', msg, msg_box.Yes | msg_box.Cancel)

        if ret == msg_box.Yes:
            self.set_game_menu_central_widget()

    def triggered_restart(self):
        msg_box = QMessageBox()
        msg_box.setWindowTitle('Restart Game - Cathedral Game')
        msg = "Do you really want to restart a new game?\nThe current progress will be lost."
        ret = msg_box.question(self, '', msg, msg_box.Yes | msg_box.Cancel)

        if ret == msg_box.Yes:
            self.set_game_central_widget(
                *player_attributes[self.centralWidget().player1.__class__.__name__], self.centralWidget().player1.name,
                *player_attributes[self.centralWidget().player2.__class__.__name__], self.centralWidget().player2.name,
            )

    def triggered_load(self):
        ...

    def triggered_save(self):
        ...

    def triggered_save_as(self):
        ...

    def triggered_undo(self):
        # Skip if we've reached the pointer's lower limit
        if self.centralWidget().board_frame.undo_pointer <= 0:
            return

        self.centralWidget().ui.board_frame.undo()

    def triggered_redo(self):
        # Skip if we've reached the pointer's upper limit
        if self.centralWidget().ui.board_frame.undo_pointer >= len(self.centralWidget().ui.board_frame.children()) - 1:
            return

        self.centralWidget().ui.board_frame.redo()

    def closeEvent(self, event):
        msg_box = QMessageBox()
        msg_box.setWindowTitle('Quit Game - Cathedral Game')
        msg = "Do you really want to quit the game?\nThe current progress will be lost."
        ret = msg_box.question(self, '', msg, msg_box.Yes | msg_box.Cancel)

        if ret == msg_box.Yes:
            event.accept()
        else:
            event.ignore()

    @staticmethod
    def triggered_about():
        dialog = QDialog()
        dialog.setWindowTitle('About Cathedral - Cathedral Game')
        ui = Ui_AboutDialog()
        ui.setupUi(dialog)
        dialog.exec_()


def main():
    app = QApplication(sys.argv)
    _ = MainWindow()
    app.exec_()


if __name__ == '__main__':
    main()
