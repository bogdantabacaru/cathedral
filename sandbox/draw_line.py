from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QPen, QBrush
from PyQt5.QtWidgets import QApplication, QFrame


class Frame(QFrame):
    def __init__(self):
        super().__init__()

        self.setMinimumHeight(200)
        self.setMinimumWidth(200)

    def paintEvent(self, e):
        super().paintEvent(e)

        painter = QPainter(self)
        painter.setPen(QPen(Qt.black, 1, Qt.SolidLine))

        painter.drawLine(10, 10, 140, 100)

        painter.drawLine(10, 10, 10, 100)
        painter.drawLine(10, 100, 30, 100)

        painter.setPen(QPen(Qt.black, 1, Qt.NoPen))
        painter.setBrush(QBrush(Qt.red, Qt.SolidPattern))
        painter.drawRect(100, 100, 100, 100)

        painter.setPen(QPen(Qt.blue, 1, Qt.SolidLine))
        painter.setBrush(QBrush(Qt.blue, Qt.NoBrush))
        painter.drawLine(100, 100, 200, 100)


def main():
    app = QApplication([])
    frame = Frame()
    frame.show()
    app.exec_()


if __name__ == '__main__':
    main()
