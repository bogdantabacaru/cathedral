import sys

from PyQt5.QtWidgets import QApplication, QWidget

from sandbox.resize_graphics.widget import Ui_Form


class Widget(QWidget):

    def __init__(self):
        super().__init__()

        self.setObjectName('Resize Graphics')

        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.show()


def main():
    app = QApplication(sys.argv)
    _ = Widget()
    app.exec_()


if __name__ == '__main__':
    main()
