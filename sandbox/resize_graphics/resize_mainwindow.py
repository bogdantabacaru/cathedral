import sys

from PyQt5.QtWidgets import QApplication, QMainWindow

from sandbox.resize_graphics.mainwindow import Ui_MainWindow


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.setObjectName('Resize Graphics')

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.show()

    def resizeEvent(self, a0):
        print('resized')


def main():
    app = QApplication(sys.argv)
    _ = MainWindow()
    app.exec_()


if __name__ == '__main__':
    main()
