import sys
import pprint
import random
import numpy as np

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QBrush, QPen
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QMainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.grid_width = 10
        self.grid_height = 10
        self.contents = np.zeros(shape=(self.grid_height, self.grid_width))
        for _ in range(70):
            self.contents[random.randint(0, self.grid_height - 1), random.randint(0, self.grid_width - 1)] = 1

        print(self.contents)

        self.calculate_regions(self.contents)

        # with open('file.csv', 'w') as f:
        #     for i in range(self.grid_width):
        #         f.write(','.join([str(self.contents[i, j]) for j in range(self.grid_height)]) + '\n')

        self.setWindowTitle('Cathedral - Game')

        self.resize(300, 300)

        # Center the window
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move(int((screen.width() - size.width()) / 2), int((screen.height() - size.height()) / 2))

        self.show()

    def descend(self, grid, row, column):
        region = []

        rows, columns = grid.shape

        if row < 0 or row >= rows or column < 0 or column >= columns:
            return region

        value = grid[row, column]

        if value == 1 or value == -1:
            return region

        # Mark as visited
        grid[row, column] = -1

        region = [(row, column)]

        for r, c in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
            region += self.descend(grid, row + r, column + c)

        return region

    def calculate_regions(self, grid):
        regions = []

        for row, column in np.ndindex(grid.shape):
            region = self.descend(grid, row, column)
            if region:
                regions.append(region)

        pprint.pprint(regions)

    def paintEvent(self, e):
        super().paintEvent(e)

        tile_size = int(self.geometry().height() / self.grid_height)

        painter = QPainter(self)
        painter.setPen(QPen(Qt.black, 1, Qt.SolidLine))

        for row, column in np.ndindex(self.contents.shape):
            color = self.contents[row, column]
            if color == 1:
                painter.setBrush(QBrush(Qt.darkBlue, Qt.SolidPattern))
            elif color == -1:
                painter.setBrush(QBrush(Qt.green, Qt.SolidPattern))
            elif color == -2:
                painter.setBrush(QBrush(Qt.darkGreen, Qt.SolidPattern))
            else:
                painter.setBrush(QBrush(Qt.white, Qt.SolidPattern))
            painter.drawRect(row * tile_size, column * tile_size, tile_size, tile_size)


def main():
    app = QApplication(sys.argv)
    _ = MainWindow()
    app.exec_()


if __name__ == '__main__':
    main()
