import sys
import csv
import pprint
import numpy as np

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QBrush, QPen, QColor
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QMainWindow


BLACK_PEN = QPen(Qt.black, 1, Qt.SolidLine)
DEFAULT_BRUSH = QBrush(Qt.white, Qt.SolidPattern)

BRUSHES_BY_ID = {
    1: QBrush(QColor(0, 158, 115), Qt.SolidPattern),  # Player 1 color
    2: QBrush(QColor(230, 159, 0), Qt.SolidPattern),  # Player 2 color
}


def read_csv(filename):
    with open(filename, 'r') as f:
        return np.array(list(csv.reader(f, delimiter=',', quotechar='|')), dtype=float)


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.grid_width = 10
        self.grid_height = 10

        # self.contents = read_csv('board_player1_wall.csv')
        # self.contents = read_csv('board_no_regions.csv')
        self.contents = read_csv('board_player1_wall_player2_pieces.csv')

        # Calculate first player's regions
        self.regions = self.calculate_regions(1, 2)

        self.setWindowTitle('Cathedral - Game')

        self.resize(300, 300)

        # Center the window
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move(int((screen.width() - size.width()) / 2), int((screen.height() - size.height()) / 2))

        self.show()

    def traverse(self, row, column, visited, player_id=1, other_player_id=2):
        region = []

        rows, columns = self.contents.shape

        # Stop traversing if we're going out of bounds
        if row < 0 or row >= rows or column < 0 or column >= columns:
            return region

        # Stop traversing if we've visited this tile before or if we are hitting player's wall
        if visited[row, column] or self.contents[row, column] == player_id:
            return region

        visited[row, column] += 1

        region = [(row, column)]

        for r in [-1, 0, 1]:
            for c in [-1, 0, 1]:
                if r == c == 0:
                    continue

                region += self.traverse(row + r, column + c, visited, player_id, other_player_id)

        return region

    def calculate_regions(self, player_id, other_player_id):
        """
        Determine which tiles of the same color (tiles which belong to the same player) form a contiguous region.
        Valid regions are surrounded entirely by the pieces of another player or are surrounded by a contiguous wall of
        the other player's tiles and one or more of the board's edges.
        """

        regions = []

        visited = np.zeros_like(self.contents)

        for row, column in np.ndindex(*self.contents.shape):
            region = self.traverse(row, column, visited, player_id, other_player_id)
            if region:
                regions.append(region)

        # Validate regions
        # If only one region found, no valid wall has been created yet
        if len(regions) < 2:
            regions = []

        valid_regions = [False] * len(regions)
        for region_index, region in enumerate(regions):
            # Update: make pieces not tile colors
            opponent_pieces = []

            for i, j in region:
                if self.contents[i, j] != other_player_id:
                    continue

                opponent_pieces.append(self.contents[i, j])

            valid_regions[region_index] = len(opponent_pieces) < 2

        regions = [region for i, region in enumerate(regions) if valid_regions[i]]

        pprint.pprint(regions)
        print(visited)
        print(len(regions))

        return regions

    def paintEvent(self, e):
        super().paintEvent(e)

        tile_size = int(self.geometry().height() / self.grid_height)

        painter = QPainter(self)
        painter.setPen(BLACK_PEN)

        # Draw the pieces
        for row, column in np.ndindex(*self.contents.shape):
            painter.setBrush(BRUSHES_BY_ID.get(self.contents[row, column], DEFAULT_BRUSH))
            painter.drawRect(column * tile_size, row * tile_size, tile_size, tile_size)

        # Draw the regions
        for region in self.regions:
            for x, y in region:
                painter.setPen(BLACK_PEN)
                painter.setBrush(QBrush(Qt.black, Qt.SolidPattern))
                painter.drawEllipse((y + 0.5) * tile_size, (x + 0.5) * tile_size, tile_size / 10, tile_size / 10)


def main():
    app = QApplication(sys.argv)
    _ = MainWindow()
    app.exec_()


if __name__ == '__main__':
    main()
