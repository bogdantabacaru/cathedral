import sys
import csv
import pprint
import numpy as np

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QBrush, QPen, QColor
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QMainWindow


BLACK_PEN = QPen(Qt.black, 1, Qt.SolidLine)
DEFAULT_BRUSH = QBrush(Qt.white, Qt.SolidPattern)

BRUSHES_BY_ID = {
    -1: QBrush(QColor(0, 158, 115), Qt.Dense3Pattern),  # Player 1 visited
    -2: QBrush(QColor(230, 159, 0), Qt.Dense3Pattern),  # Player 2 visited
    1: QBrush(QColor(0, 158, 115), Qt.SolidPattern),  # Player 1 color
    2: QBrush(QColor(230, 159, 0), Qt.SolidPattern),  # Player 2 color
}


def read_csv(filename):
    with open(filename, 'r') as f:
        return np.array(list(csv.reader(f, delimiter=',', quotechar='|')), dtype=float)


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.grid_width = 10
        self.grid_height = 10

        self.contents = read_csv('board.csv')

        self.calculate_regions()

        self.setWindowTitle('Cathedral - Game')

        self.resize(300, 300)

        # Center the window
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move(int((screen.width() - size.width()) / 2), int((screen.height() - size.height()) / 2))

        self.show()

    def descend(self, row, column):
        region = []

        rows, columns = self.contents.shape

        if row < 0 or row >= rows or column < 0 or column >= columns:
            return region

        value = self.contents[row, column]

        if value in [-1, 1]:
            return region

        # Mark as visited
        self.contents[row, column] = -1

        region = [(row, column)]

        for r in [-1, 0, 1]:
            for c in [-1, 0, 1]:
                if r == c == 0:
                    continue

                region += self.descend(row + r, column + c)

        return region

    def calculate_regions(self):
        regions = []

        for row, column in np.ndindex(*self.contents.shape):
            region = self.descend(row, column)
            if region:
                regions.append(region)

        pprint.pprint(regions)
        print(len(regions))

    def paintEvent(self, e):
        super().paintEvent(e)

        tile_size = int(self.geometry().height() / self.grid_height)

        painter = QPainter(self)
        painter.setPen(BLACK_PEN)

        # Draw the pieces
        for row, column in np.ndindex(*self.contents.shape):
            painter.setBrush(BRUSHES_BY_ID.get(self.contents[row, column], DEFAULT_BRUSH))
            painter.drawRect(column * tile_size, row * tile_size, tile_size, tile_size)


def main():
    app = QApplication(sys.argv)
    _ = MainWindow()
    app.exec_()


if __name__ == '__main__':
    main()
