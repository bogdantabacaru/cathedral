import os

from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent

THIS_PATH = os.path.dirname(__file__)
SOUNDS_DIR = os.sep.join([THIS_PATH, '..', 'sounds'])

SOUNDS = {
    '1': QMediaContent(QUrl.fromLocalFile(os.sep.join([SOUNDS_DIR, 'ES_Toy Wood Block 1 - SFX Producer.mp3']))),
    '2': QMediaContent(QUrl.fromLocalFile(os.sep.join([SOUNDS_DIR, 'ES_Wood Object Down 3 - SFX Producer.mp3']))),
    '3': QMediaContent(QUrl.fromLocalFile(os.sep.join([SOUNDS_DIR, 'ES_Wood Object Down 4 - SFX Producer.mp3']))),
}

MEDIA_PLAYER = QMediaPlayer()


def play(sound_id):
    print('Play sound', sound_id)
    MEDIA_PLAYER.setMedia(SOUNDS[sound_id])
    MEDIA_PLAYER.play()


def main():
    app = QApplication([])

    button1 = QPushButton('1')
    button2 = QPushButton('2')
    button3 = QPushButton('3')
    button1.clicked.connect(lambda: play(button1.text()))
    button2.clicked.connect(lambda: play(button2.text()))
    button3.clicked.connect(lambda: play(button3.text()))

    layout = QHBoxLayout()
    layout.addWidget(button1)
    layout.addWidget(button2)
    layout.addWidget(button3)

    widget = QWidget()
    widget.setLayout(layout)

    widget.show()

    app.exec_()


if __name__ == '__main__':
    main()
