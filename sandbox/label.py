from PyQt5.QtWidgets import QApplication, QLabel


class Label(QLabel):
    def __init__(self, text):
        super().__init__(text)

        self.setMinimumHeight(30)
        self.setMinimumWidth(30)

    def paintEvent(self, e):
        super().paintEvent(e)

        self.setText('h: {}, w: {}'.format(self.height(), self.width()))


def main():
    app = QApplication([])
    label = Label('hello')
    label.show()
    app.exec_()


if __name__ == '__main__':
    main()
