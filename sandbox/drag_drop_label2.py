import sys

from PyQt5.QtWidgets import QApplication, QLabel, QWidget
from PyQt5.QtGui import QDrag, QPixmap, QPainter
from PyQt5.QtCore import QMimeData, Qt


class DraggableLabel(QLabel):
    def mouseMoveEvent(self, event):
        pixmap = QPixmap(self.size())
        painter = QPainter(pixmap)
        painter.drawPixmap(self.rect(), self.grab())
        painter.end()

        mimedata = QMimeData()
        mimedata.setText(self.text())

        drag = QDrag(self)
        drag.setMimeData(mimedata)
        drag.setPixmap(pixmap)
        drag.setHotSpot(event.pos())
        drag.exec_(Qt.MoveAction)


class Widget(QWidget):
    def __init__(self):
        super().__init__()

        self.label_to_drag = DraggableLabel("drag this", self)

        self.initUI()

    def initUI(self):
        self.setAcceptDrops(True)

        self.setGeometry(300, 300, 550, 450)

        self.show()

    def dragEnterEvent(self, e):
        e.accept()

    def dropEvent(self, e):
        position = e.pos()
        self.label_to_drag.move(position)
        e.setDropAction(Qt.MoveAction)
        e.accept()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Widget()
    w.show()
    sys.exit(app.exec_())
