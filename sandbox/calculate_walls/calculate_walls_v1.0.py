import sys
import csv
import pprint
import numpy as np

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QBrush, QPen, QColor
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QMainWindow


BLACK_PEN = QPen(Qt.black, 1, Qt.SolidLine)
DEFAULT_BRUSH = QBrush(Qt.white, Qt.SolidPattern)

BRUSHES_BY_ID = {
    1: QBrush(QColor(0, 158, 115), Qt.SolidPattern),  # Player 1 color
    2: QBrush(QColor(230, 159, 0), Qt.SolidPattern),  # Player 2 color
}


def read_csv(filename):
    with open(filename, 'r') as f:
        return np.array(list(csv.reader(f, delimiter=',', quotechar='|')), dtype=float)


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.grid_width = 10
        self.grid_height = 10

        self.contents = read_csv('board.csv')

        # Calculate player1's walls
        # self.player1_walls = [[(0, 0), (0, 1), (0, 2)]]
        self.player1_walls = self.calculate_walls(1, 2)

        # Calculate player2's walls
        # self.player2_walls = self.calculate_walls(2, 1)

        self.setWindowTitle('Cathedral - Game')

        self.resize(300, 300)

        # Center the window
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move(int((screen.width() - size.width()) / 2), int((screen.height() - size.height()) / 2))

        self.show()

    def traverse(self, row, column, visited, player_id=1, other_player_id=2):
        wall = []

        rows, columns = self.contents.shape

        # Stop traversing if we're going out of bounds
        if row < 0 or row >= rows or column < 0 or column >= columns:
            return wall

        # Stop traversing if we've visited this tile before or if we are hitting the other player's wall
        if visited[row, column] or self.contents[row, column] != player_id:
            return wall

        visited[row, column] += 1

        wall = [(row, column)]
        wall += self.traverse(row - 1, column, visited, player_id, other_player_id)
        wall += self.traverse(row + 1, column, visited, player_id, other_player_id)
        wall += self.traverse(row, column - 1, visited, player_id, other_player_id)
        wall += self.traverse(row, column + 1, visited, player_id, other_player_id)

        return wall

    def calculate_walls(self, player_id, other_player_id):
        """
        Determine which tiles of the same color (tiles which belong to the same player) form a contiguous wall.
        Valid walls are connected to the board's edge(s) or are cyclic.
        Valid walls are connected left, right, up, or down (diagonals are not relevant).
        Valid walls must contain one or more tiles of a different color.

        The minimum length of a valid cyclic wall is a ring of 8 tiles.
        The minimum length of a valid wall which hits two different board edges is 3 tiles.
        The minimum length of a valid wall which hits the same board edge is 5 tiles.
        """

        walls = []

        visited = np.zeros_like(self.contents)

        for row, column in np.ndindex(*self.contents.shape):
            wall = self.traverse(row, column, visited, player_id, other_player_id)
            # This is an optimization
            if len(wall) > 2:
                walls.append(wall)

        pprint.pprint(walls)
        print(visited)

        # Validated walls
        # Detect cycles
        # Detect nodes which are touching different board edges
        # Detect nodes which are touching the same board edge

        return walls

    def paintEvent(self, e):
        super().paintEvent(e)

        tile_size = int(self.geometry().height() / self.grid_height)

        painter = QPainter(self)
        painter.setPen(BLACK_PEN)

        # Draw the pieces
        for row, column in np.ndindex(*self.contents.shape):
            painter.setBrush(BRUSHES_BY_ID.get(self.contents[row, column], DEFAULT_BRUSH))
            painter.drawRect(column * tile_size, row * tile_size, tile_size, tile_size)

        # Draw the walls
        for wall in self.player1_walls:
            for x, y in wall:
                painter.setPen(BLACK_PEN)
                painter.setBrush(QBrush(Qt.black, Qt.SolidPattern))
                painter.drawEllipse((y + 0.5) * tile_size, (x + 0.5) * tile_size, tile_size / 10, tile_size / 10)


def main():
    app = QApplication(sys.argv)
    _ = MainWindow()
    app.exec_()


if __name__ == '__main__':
    main()
