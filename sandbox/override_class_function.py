class A:

    def disp(self):
        print('A')


class B(A):

    def disp(self):
        print('B')
        # self.disp = super().disp  # Works too but PyCharm complains about not defining self.disp in __init__.
        setattr(self, 'disp', super().disp)


def main():
    b = B()
    b.disp()  # --> B1
    b.disp()  # --> A
    b.disp()  # --> A


if __name__ == '__main__':
    main()
