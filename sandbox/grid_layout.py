import sys

from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit, QVBoxLayout, QPushButton, QGridLayout


class GridLayoutExample(QWidget):
    def __init__(self):
        super().__init__()

        label1 = QLabel('label 1')
        line_edit1 = QLineEdit()
        sublayout1 = QVBoxLayout()
        sublayout1.addWidget(label1)
        sublayout1.addWidget(line_edit1)

        label2 = QLabel('label 2')
        line_edit2 = QLineEdit()
        sublayout2 = QVBoxLayout()
        sublayout2.addWidget(label2)
        sublayout2.addWidget(line_edit2)

        button1 = QPushButton("button1")
        button2 = QPushButton("button2")
        button3 = QPushButton("button3")

        grid_layout = QGridLayout(self)
        grid_layout.addLayout(sublayout1, 0, 0, 1, 3)
        grid_layout.addLayout(sublayout2, 1, 0, 1, 3)
        grid_layout.addWidget(button1, 2, 0, 1, 1)
        grid_layout.addWidget(button2, 2, 1, 1, 1)
        grid_layout.addWidget(button3, 2, 2, 1, 1)

        self.show()


def main():
    app = QApplication(sys.argv)
    _ = GridLayoutExample()
    app.exec_()


if __name__ == '__main__':
    main()
