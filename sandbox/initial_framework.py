import os
import sys
import numpy as np

from PyQt5.QtCore import Qt, QMimeData
from PyQt5.QtGui import QDrag
from PyQt5.QtWidgets import QPushButton, QWidget, QApplication, QLabel

THIS_DIR = os.path.dirname(__file__)
PIECES_DIR = os.sep.join([THIS_DIR, 'pieces'])

TILE_LENGTH = 3


class Tile(QLabel):

    def mousePressEvent(self, e):
        if e.buttons() != Qt.LeftButton:
            return

        mimeData = QMimeData()

        drag = QDrag(self)
        drag.setMimeData(mimeData)
        drag.setHotSpot(e.pos() - self.rect().topLeft())

        dropAction = drag.exec_(Qt.MoveAction)


class Player:
    def __init__(self, name, color):
        self.name = name
        self.color = color
        self.pieces = [
            # os.sep.join([PIECES_DIR, x + '.csv']) for x in [
            #     'one', 'one', 'two', 'two', 'three_square', 'three_square', 'three', 'square',
            # ]
        ]

    def is_winner(self):
        return False

    def has_valid_move(self):
        return not self.pieces

    def do_turn(self):
        # choose piece
        # place piece (move, rotate)
        # end turn
        # self.game.end_turn()
        pass


class Player1(Player):
    def __init__(self):
        super().__init__('player1', 'yellow')
        # Add the cathedral
        self.pieces.insert(0, os.sep.join([PIECES_DIR, 'cathedral.csv']))


class Player2(Player):
    def __init__(self):
        super().__init__('player2', 'red')


class Game(QWidget):
    def __init__(self):
        super().__init__()

        self.players = [Player1(), Player2()]

        self.initUI()

        self.play()
        self.announce_winner()

    def next_turn(self):
        for player in self.players:
            self.player.do_turn()

            # self.update()

            if player.is_winner():
                return False

            if self.is_over():
                return False

        return True

    def play(self):
        while True:
            if not self.next_turn():
                break

    def is_over(self):
        for player in self.players:
            if not player.has_valid_move():
                return True
        else:
            return False

    def announce_winner(self):
        print('Player1 has one')

    def initUI(self):
        self.setAcceptDrops(True)

        self.cathedral.relocate(100, 65)

        self.setWindowTitle('Cathedral - Game')
        self.setGeometry(300, 300, 550, 450)

    def dragEnterEvent(self, e):
        e.accept()

    def dropEvent(self, e):
        position = e.pos()
        self.cathedral.relocate(position)

        e.setDropAction(Qt.MoveAction)
        e.accept()


def main():
    app = QApplication(sys.argv)
    game = Game()
    game.show()
    app.exec_()


if __name__ == '__main__':
    main()
