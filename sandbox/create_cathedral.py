import os
import sys
import numpy as np

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QBrush, QPen
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QMainWindow, QFrame, QLabel

THIS_DIR = os.path.dirname(__file__)
PIECES_DIR = os.sep.join([THIS_DIR, '..', 'pieces'])


class Cathedral(QLabel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.shape = np.genfromtxt(os.sep.join([PIECES_DIR, 'cathedral.csv']), delimiter=',')
        self.color = Qt.darkRed


class Board(QFrame):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.height = 10
        self.width = 10
        self.tile_size = 50  # pixels
        self.contents = {}

        self.active_piece = Cathedral(self)
        self.active_x = 0
        self.active_y = 0

    def paintEvent(self, e):
        painter = QPainter(self)
        painter.setPen(QPen(Qt.black, 1, Qt.SolidLine))
        painter.setBrush(QBrush(Qt.white, Qt.SolidPattern))

        # Draw the empty board
        for x in range(self.width):
            for y in range(self.height):
                painter.drawRect(x * self.tile_size, y * self.tile_size, self.tile_size, self.tile_size)

        # Draw the active piece
        for y, x in np.ndindex(self.active_piece.shape.shape):
            if self.active_piece.shape[y, x]:
                painter.setBrush(QBrush(self.active_piece.color, Qt.SolidPattern))
                painter.drawRect(
                    (self.active_x + x) * self.tile_size, (self.active_y + y) * self.tile_size,
                    self.tile_size, self.tile_size)

    def keyPressEvent(self, e):
        key = e.key()

        if key == Qt.Key_Left:
            self.try_move(-1, 0)

        elif key == Qt.Key_Right:
            self.try_move(1, 0)

        elif key == Qt.Key_Up:
            self.try_move(0, -1)

        elif key == Qt.Key_Down:
            self.try_move(0, 1)

        elif key == Qt.Key_Tab:
            self.rotate_right()

        elif key == Qt.Key_Backtab:
            self.rotate_left()

        else:
            super().keyPressEvent(e)

        self.update()

    def bump_piece(self, collision, pos=1):
        """ Move/Bump piece up, down, left, or right by [pos] steps"""

        if collision == "left":
            self.active_x += pos
        elif collision == "right":
            self.active_x -= pos
        if collision == "up":
            self.active_y += pos
        elif collision == "down":
            self.active_y -= pos

    def has_collided(self, x0=0, y0=0):
        """ Return the side(s) of the board with which the piece has collided """

        collisions = set()

        for y, x in np.ndindex(self.active_piece.shape.shape):
            new_x = self.active_x + x + x0
            new_y = self.active_y + y + y0

            if new_x < 0:
                collisions.add('left')
            if self.width <= new_x:
                collisions.add('right')
            if new_y < 0:
                collisions.add('up')
            if self.height <= new_y:
                collisions.add('down')

        return collisions

    def try_move(self, x=0, y=0):
        if not self.has_collided(x, y):
            self.active_x += x
            self.active_y += y

    def rotate_left(self):
        self.active_piece.shape = np.rot90(self.active_piece.shape)
        list(map(self.bump_piece, self.has_collided()))

    def rotate_right(self):
        self.active_piece.shape = np.rot90(self.active_piece.shape, k=-1)
        list(map(self.bump_piece, self.has_collided()))


class CathedralGame(QMainWindow):
    def __init__(self):
        super().__init__()

        self.board = Board(self)
        self.setCentralWidget(self.board)
        self.board.setFocusPolicy(Qt.StrongFocus)

        self.setWindowTitle('Cathedral - Game')
        self.resize(self.board.width * self.board.tile_size, self.board.height * self.board.tile_size)

        # Center
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move(int((screen.width() - size.width()) / 2), int((screen.height() - size.height()) / 2))

        self.show()


def main():
    app = QApplication(sys.argv)
    _ = CathedralGame()
    app.exec_()


if __name__ == '__main__':
    main()
