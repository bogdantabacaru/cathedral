import os
import csv
import numpy as np

THIS_DIR = os.path.dirname(__file__)
PIECES_DIR = os.sep.join([THIS_DIR, 'resources', 'pieces'])


def read_csv(filename):
    with open(filename, 'r') as f:
        return np.array(list(csv.reader(f, delimiter=',', quotechar='|')), dtype=float)


class Piece:

    def __init__(self):
        self.player = None
        self.name = None
        self.shape = None
        self.x0 = 0
        self.y0 = 0
        self.inventory_ref = None

    def set_shape(self, name):
        self.player, self.name = name.replace('_label', '').split('_')
        self.player = self.player.replace('player', '')
        self.name = self.name.replace('1', '').replace('2', '')
        self.shape = read_csv(os.sep.join([PIECES_DIR, 'player' + self.player, self.name + '.csv']))

    def rotate(self, rotation_factor):
        for _ in range(abs(rotation_factor)):
            self.shape = np.rot90(self.shape, -np.sign(rotation_factor))

    def is_out_of_bounds(self, board_frame, x=0, y=0):
        """ Check if the piece is trying to move passed the board's border """

        for i, j in np.ndindex(*self.shape.shape):
            new_x, new_y = self.x0 + j + x, self.y0 + i + y

            if new_x < 0 or new_x >= board_frame.grid_width or new_y < 0 or new_y >= board_frame.grid_height:
                return True

        return False

    def nudge(self, board_frame):
        """ If the piece is out of bounds, nudge it in different directions until it's back in bounds """

        dx = dy = 0

        for di in [0, -1, -2, 1, 2]:
            found = False

            for dj in [0, -1, -2, 1, 2]:
                if not self.is_out_of_bounds(board_frame, di, dj):
                    dx, dy, found = di, dj, True
                    break

            if found:
                break

        return dx, dy

    def relocate(self, x=0, y=0):
        self.x0 += x
        self.y0 += y
